const express = require('express')
const app = express()
const fs = require('fs')

app.get('/', (req, res) => {
  const {
    HELLO_MESSAGE,
    FILE_LOCATION
  } = process.env
  const message = HELLO_MESSAGE || 'Hello World!'

  let fileMessage = 'There are no files in this location'
  if (fs.existsSync(FILE_LOCATION)) {
    fileMessage = fs.readFileSync(FILE_LOCATION);
  }

  res.send(`${message}<br />${fileMessage}`)
})

app.listen(3000, () => console.log('Example app listening on port 3000!'))